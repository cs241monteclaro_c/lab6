﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sortsLib
{
    public class HelperFuncs
    {
        // Sedgewick provided these HelperFuncs

        // Straightforward so no explanation

        public static void Exchange(IComparable[] test, int current, int inFrontOf)
        {
            IComparable change = test[current];
            test[current] = test[inFrontOf];
            test[inFrontOf] = change;
        }

        public static bool IsSorted(IComparable[] sortedCase)
        {
            bool returnVal = true;
            for (int i = 1; i < sortedCase.Length; i++)
            {
                if (Less(sortedCase[i], sortedCase[i - 1]))
                {
                    returnVal = false;
                }
            }

            return returnVal;
        }

        public static bool Less(IComparable current, IComparable Next)
        {
            return current.CompareTo(Next) < 0;
        }
        
        public static void Show(IComparable[] testCase)
        {
            for (int i = 0; i < testCase.Length; i++)
            {
                Console.WriteLine(testCase[i] + " ");
            }
        }

    }
}
