﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sortsLib
{
    public class Insertion
    {
        public static void NonSentinelSort(IComparable[] testCase)
        {
            
            int caseLength = testCase.Length;

            // Outer loop handles the second element of the
            // provided array.

            for (int InFront = 1; InFront < caseLength; InFront++)
            {

                // Inner loop handles the testing to see if
                // the current cursor is greater than
                // the element before it.

                for (int cursor = InFront;
                    cursor > 0 &&
                    HelperFuncs.Less(testCase[cursor], testCase[cursor - 1]);
                    cursor--)
                {
                    HelperFuncs.Exchange(testCase, cursor, cursor - 1);
                }
            }

            // If it is, then it'll switch it.
            // If it isn't, it breaks the loop.
        }


        // Same as above
        // but instead of case by case exchanging
        // we use a sentinel to chunk and move elements.
        // More efficient.
        public static void SentinelSort(IComparable[] testCase)
        {
            int caseLength = testCase.Length;


            // Finds smallest element in the array
            // and sets it to the first case,
            // eliminating need for greater
            // than check.

            for(int i = 0; i < testCase.Length; i++)
            {
                if(testCase[i] == testCase.Min())
                {
                    IComparable exchange = testCase[0];
                    testCase[0] = testCase[i];
                    testCase[i] = exchange;
                    break;
                }
            }
            

            for (int InFront = 1; InFront < caseLength; InFront++)
            {
                for (int cursor = InFront;
                    HelperFuncs.Less(testCase[cursor], testCase[cursor - 1]);
                    cursor--)
                {
                    HelperFuncs.Exchange(testCase, cursor, cursor - 1);
                }
            }
        }
    }
}
