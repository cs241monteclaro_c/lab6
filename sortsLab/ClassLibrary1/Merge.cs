﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sortsLib
{
    public class Merge
    {
        // Auxillary array to handle
        // the seperate halves of the merge
        private static IComparable[] aux;

        // Call Function
        public static void Sort(IComparable[] test)
        {
            aux = new IComparable[test.Length];
            TopDown(test, 0, aux.Length - 1);

        }

        // Processing Function
        private static void Sorting(IComparable[] test, int lo, int mid, int hi)
        {
            int i = lo, j = mid + 1;

            // Copies values of test into the
            // auxillary
            for(int k = lo; k<= hi; k++)
            {
                aux[k] = test[k];
            }

            for(int k = lo; k <= hi; k++)
            {
                // if lo > mid
                if (i > mid)
                {
                    test[k] = aux[j++];
                }
                
                // right of the middle value > hi
                else if (j > hi)
                {
                    test[k] = aux[i++];
                }

                // if middle is greater than low
                else if (HelperFuncs.Less(aux[j], aux[i]))
                {
                    test[k] = aux[j++];
                }

                // if its where it needs to be
                else
                {
                    test[k] = aux[i++];
                }
            }

        }
        
        // Note: Top Down Implementation Function
        // Not Bottom Up
        private static void TopDown(IComparable[] test, int lo, int hi)
        {
            // Conditional that stops program
            // if somehow the hi ends up being the lowest value

            // Here to ensure consistency
            if (hi <= lo)
            {
                return;   
            }

            // sets the midway value where
            // the two arrays will join
            int mid = lo + (hi - lo) / 2;

            // Sorts left.
            TopDown(test, lo, mid);

            // Sorts right.
            TopDown(test, mid + 1, hi);

            // Merges the two together.
            Sorting(test, lo, mid, hi);

        }


    }
}
