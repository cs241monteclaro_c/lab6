﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sortsLib
{
    public class Selection
    {
        public static void Sort(IComparable[] testCase)
        {
            // Counter set to length
            int n = testCase.Length;

            // Outer loop is cursor pointing out
            // current item
            for (int cursor = 0; cursor < n; cursor++)
            {
                // minimum value set to current cursor 
                int min = cursor;

                // Inner loop looks at the next item in the array
                for (int nextCursor = cursor + 1; nextCursor < n; nextCursor++)
                {
                    // Use of helper function for IComparable interface
                    // Compares the value of the next item
                    // to the value of the current item being held
                    // If the next is less than the current,
                    // set it as the new minimum value
                    if (HelperFuncs.Less(testCase[nextCursor], testCase[min]))
                    {
                        min = nextCursor;
                    }

                }

                // Helper function for exchanging values
                // Will 
                HelperFuncs.Exchange(testCase, cursor, min);
            }
        }

        
    }
}
