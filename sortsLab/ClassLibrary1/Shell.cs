﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sortsLib
{
    public class Shell
    {
        public static void Sort(IComparable[] testCase)
        {
            // Implement Knuth algorithm 
            int caseLength = testCase.Length;
            int stride = 1;

            //Set the stride to about n/9
            while (stride < (caseLength / 3))
            {
                stride = 3 * stride + 1;
            }
            
            // While loop to 
            // maintain until stride reaches 1
            while (stride >= 1)
            {
                // Actual stride sorting 
                for (int i = stride; i < caseLength; i++)
                {
                    // inserts index of i among the strides
                    for (int cursor = i;
                    cursor >= stride &&
                    HelperFuncs.Less(testCase[cursor], testCase[cursor - stride]);
                    cursor -= stride)
                    {
                        HelperFuncs.Exchange(testCase, cursor, cursor - stride);
                    }

                }

                // Brings stride down
                stride = stride / 3;
            }

        }
    }
}
