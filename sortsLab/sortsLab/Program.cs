﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sortsLib;
using System.Diagnostics;

namespace sortsLab
{
    public class Program
    {
        public static void Main(string[] args)
        {

            IComparable[] caseOne = { 'a', 'g', 'c', 'f', 'k' };
            IComparable[] caseTwo = new IComparable[5000];
            IComparable[] caseThree = { "Kenny", "Alestar", "Rei", "Soma", "Alistair" };
            IComparable[] caseFour = { 0.32, 1.45, 100.00, 30.14, 0.01 };
            IComparable[] caseFive = caseTwo;

            int average = 0;

            Stopwatch timer = new Stopwatch();
            Random rand = new Random();

            for(int i = 0; i < caseTwo.Length; i++)
            {
                caseTwo[i] = rand.Next();
            }
            
            ////
            Console.WriteLine("Selection ------\n");
                       
            timer.Start();
            Selection.Sort(caseOne);
            timer.Stop();

            Console.WriteLine(HelperFuncs.IsSorted(caseOne));
            HelperFuncs.Show(caseOne);

            Console.WriteLine("\nElapsed Ticks:" + timer.ElapsedTicks);
            timer.Reset();

            ////
            Console.WriteLine("\nInsertionNonSentinel ------\n");

            timer.Start();
            Insertion.NonSentinelSort(caseTwo);
            timer.Stop();

            Console.WriteLine(HelperFuncs.IsSorted(caseTwo));

            Console.WriteLine("\nElapsed Ticks:" + timer.ElapsedTicks);
            timer.Reset();

            ////
            Console.WriteLine("\nInsertionSentinel ------\n");

            timer.Start();
            Insertion.SentinelSort(caseFive);
            timer.Stop();

            Console.WriteLine(HelperFuncs.IsSorted(caseFive));
            
            Console.WriteLine("\nElapsed Ticks:" + timer.ElapsedTicks);

            Console.WriteLine("As you can see, Insertion is much faster.");
            Console.WriteLine("However, if the length of the case is lower,");
            Console.WriteLine("It is the opposite.");
            timer.Reset();

            ////
            Console.WriteLine("\nShell ------\n");

            timer.Start();
            Shell.Sort(caseThree);
            timer.Stop();

            Console.WriteLine(HelperFuncs.IsSorted(caseThree));
            HelperFuncs.Show(caseThree);

            Console.WriteLine("\nElapsed Ticks:" + timer.ElapsedTicks);
            timer.Reset();

            ////
            Console.WriteLine("\nMerge ------\n");

            timer.Start();
            Merge.Sort(caseFour);
            timer.Stop();

            Console.WriteLine(HelperFuncs.IsSorted(caseFour));
            HelperFuncs.Show(caseFour);

            Console.WriteLine("\nElapsed Ticks:" + timer.ElapsedTicks);
            timer.Reset();

            ////
            Console.Read();
        }

    }
}
